# ~/.bashrc: executed by bash for non-login shells.

let CNT_BASHRC=$CNT_BASHRC+1; export CNT_BASHRC

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;
esac

#----------------
# EXTRAS
#----------------

# bash prompt
if [ -f ${HOME}/.bash_prompt ]; then
    source ${HOME}/.bash_prompt
fi

# Alias definition
if [ -f ${HOME}/.bash_aliases ]; then
    source ${HOME}/.bash_aliases
fi

# Functions variables
if [ -f ${HOME}/.bash_variables ]; then
    source ${HOME}/.bash_variables
fi

# Functions definition
if [ -f ${HOME}/.bash_functions ]; then
    source ${HOME}/.bash_functions
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
    elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi

# git completion
if [ -f ${HOME}/.git-completion.bash ]; then
    source ${HOME}/.git-completion.bash
fi

# git prompt
if [ -f ${HOME}/.git-prompt.sh ]; then
    source ${HOME}/.git-prompt.sh
fi

#----------------
# SHELL OPTIONs
#----------------

shopt -s extglob        # extended globbing
shopt -s globstar       # match all files and zero or more directories and subdirectories
shopt -s histappend     # append to the history file, don't overwrite it
shopt -s checkwinsize   # check the window size after each command and, if necessary, update the values of LINES and COLUMNS
shopt -s cdspell        # minor errors in the spelling of a directory component in a cd command will be corrected

#----------------
# MISC
#----------------

[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"   #make less more friendly for non-text input files, see lesspipe(1)
set -o vi   # vi mode


#----------------
# LOCAL SETTINGS
#----------------

if [ -f ${HOME}/.bashrc-local ]; then
    source ${HOME}/.bashrc-local
fi
