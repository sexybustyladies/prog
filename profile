# ~/.profile: executed by the command interpreter for login shells.
# 1- ~/.bash_profile (for the Bourne Again shell)
# 2- ~/.bash_login
# 3- ~/.profile (for the Bourne and Korn shells)

let CNT_PROFILE=$CNT_PROFILE+1; export CNT_PROFILE

### PATH
if [ -d ${HOME}/bin ] ; then
	PATH=${HOME}/bin:${PATH}
	export PATH
fi

### LIB
if [ -d ${HOME}/lib ] ; then
	LD_LIBRARY_PATH=${PATH}:${HOME}/lib
	export LD_LIBRARY_PATH
	LD_RUN_PATH=${PATH}:${HOME}/lib
	export LD_RUN_PATH
fi
if [ -d ${HOME}/lib/pkgconfig ] ; then
	PKG_CONFIG_PATH=${PATH}:${HOME}/lib/pkgconfig
	export PKG_CONFIG_PATH
fi

### EXTRA
if [ -f ${HOME}/.profile-local ]; then
    source ${HOME}/.profile-local
fi
