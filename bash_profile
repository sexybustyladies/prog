# ~/.bash_profile: executed by the command interpreter for login shells: 
# 1- ~/.bash_profile (for the Bourne Again shell)
# 2- ~/.bash_login
# 3- ~/.profile (for the Bourne and Korn shells)

let CNT_BASH_PROFILE=$CNT_BASH_PROFILE+1; export CNT_BASH_PROFILE

# Load .profile, containing login, non-bash related initializations.
if [ -f ${HOME}/.profile ]; then
	source ${HOME}/.profile
fi
 
# Load .bashrc, containing non-login related bash initializations.
if [[ -f ${HOME}/.bashrc && $- == *i* ]]; then
	source ${HOME}/.bashrc
fi
