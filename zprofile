# ~/.profile: executed by the command interpreter for login shells.

if [ -f "$HOME/.bash_profile-local" ]; then
    . "$HOME/.bash_profile-local"
fi

if [ -n "$BASH_VERSION" ]; then
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi
